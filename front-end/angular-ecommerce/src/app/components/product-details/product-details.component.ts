import { Product } from './../../common/product';
import { Route, ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { Component, Inject, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { CartItem } from 'src/app/common/cart-item';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})

export class ProductDetailsComponent implements OnInit {

  product: Product = new Product();

  constructor(private service: ProductService,
               private route: ActivatedRoute,
               private cartService: CartService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe( () => {
      this.getProductDetails();
    })
  }

  getProductDetails() {
    const theProductId = +this.route.snapshot.paramMap.get('id')!;

    this.service.getProduct(theProductId).subscribe(data => {
      this.product = data;
    })
  }

  addToCart(){
    console.log(`Adding to cart: ${this.product.name}, ${this.product.unitPrice}`);

    this.cartService.addToCart(new CartItem(this.product));
    
  }
}
