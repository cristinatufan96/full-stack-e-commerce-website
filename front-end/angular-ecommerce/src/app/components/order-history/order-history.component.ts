import { OrderHistory } from './../../common/order-history';
import { OrderHistoryService } from './../../services/order-history.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {

  storage:Storage = sessionStorage;
   orders: OrderHistory[] = [];

  constructor(private orderService: OrderHistoryService) { }

  ngOnInit(): void {
    this.handleOrderHistory();
  }

  handleOrderHistory(){
    const theEmail = JSON.parse(this.storage.getItem('userEmail'));

    console.log('The email is '+theEmail);

    this.orderService.getOrderHistory(theEmail).subscribe(
      data =>{
        this.orders = data._embedded.orders;
      });
  }

}
