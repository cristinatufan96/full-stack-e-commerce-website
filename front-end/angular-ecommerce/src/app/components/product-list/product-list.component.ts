import { CartItem } from './../../common/cart-item';
import { CartService } from '../../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/common/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  
  products: Product[] = [];
  currentCategoryId: number = 1;
  previousCategoryId: number=1;
  searchMood: boolean = false;

  previousKeyword: string = '';

  // Pagination properties
  thePageNumber: number = 1;
  thePageSize: number = 5;
  theTotalElements: number = 0;


  constructor(private service: ProductService,
              private route: ActivatedRoute,
              private cartService: CartService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(() => {
    this.listProducts();
    });
  }

  listProducts(){
    this.searchMood = this.route.snapshot.paramMap.has('keyword');
    if(this.searchMood){
      this.handleSearchProducts();
    } else{
      this.handleListProducts();
    }
  }

  handleListProducts(){
    const hasCategoryId:boolean = this.route.snapshot.paramMap.has('id');
    if(hasCategoryId){
      this.currentCategoryId = Number(this.route.snapshot.paramMap.get('id'));
    } else{
      this.currentCategoryId = 1;
    }

    // check if we have a different category than  the previous
    //Note: Angular will reuse a component if it is currently being viewed

    //if we have a different category id than previous then set thePageNumber back to 1
    if(this.previousCategoryId != this.currentCategoryId){
      this.thePageNumber = 1;
    }

    this.previousCategoryId = this.currentCategoryId;


   this.service.getProductListPaginate(
                                      this.thePageNumber -1,
                                      this.thePageSize,
                                      this.currentCategoryId)
                                      .subscribe(this.processResult());
  }

  handleSearchProducts(){
    const theKeyword: string = this.route.snapshot.paramMap.get('keyword')!;

    if(this.previousKeyword != theKeyword){
      this.thePageNumber = 1;
    }
    
    this.previousKeyword = theKeyword;

    this.service.searchProductsPaginate(this.thePageNumber -1,
                                this.thePageSize, 
                                theKeyword)
                                .subscribe(this.processResult());
  }

  processResult(){
    return (data: { _embedded: { products: Product[]; }; page: { number: number; size: number; totalElements: number; }; }) => {
      this.products = data._embedded.products;
      this.thePageNumber = data.page.number +1;
      this.thePageSize = data.page.size;
      this.theTotalElements = data.page.totalElements;
    };
  }

  updatePageSize(value: number){
    this.thePageSize = value;
    this.thePageNumber = 1;
    this.listProducts();

  }

  addToCart(product: Product){

    console.log(`Adding to cart: ${product.name}, ${product.unitPrice}`);
    

   const cartItem: CartItem = new CartItem(product);

   this.cartService.addToCart(cartItem);
    
  }

}
