import { environment } from './../../environments/environment';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { from, lastValueFrom, Observable } from 'rxjs';
import { Inject, Injectable } from '@angular/core';
import { OktaAuthStateService, OKTA_AUTH } from '@okta/okta-angular';
import { OktaAuth } from '@okta/okta-auth-js';

@Injectable({

  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(public authStateService: OktaAuthStateService, @Inject(OKTA_AUTH) public oktaAuth: OktaAuth) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return from(this.handleAccess(request, next));
  }

  private async handleAccess(request: HttpRequest<any>, next: HttpHandler): Promise<HttpEvent<any>> {


    // Only add an access token for secured endpoints
    const theEndPoint = environment.luv2codeApiUrl + '/orders';
    const securedEndpoints = [theEndPoint];

    if (securedEndpoints.some(url => request.urlWithParams.includes(url))) {

      // get access token
      const accessToken = await this.oktaAuth.getAccessToken();
      const authorizationHeader = 'Bearer ' + accessToken;

      // clone the request and add new header with access token

      request = request.clone({
        setHeaders: {
          Authorization: authorizationHeader
        }
      });

      console.log(request);
    }
    return next.handle(request).toPromise();

  }
}
