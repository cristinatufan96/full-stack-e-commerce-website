
import { CartItem } from './../common/cart-item';
import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CartService {

  cartItems: CartItem[] = [];

  totalPrice: Subject<number> = new BehaviorSubject<number>(0);
  totalQuantity: Subject<number> = new BehaviorSubject<number>(0);

  storage: Storage = sessionStorage;


  constructor() {

    //read data from storage
    let data = JSON.parse(this.storage.getItem('cartItems'));

    if(data != null){
      this.cartItems = data;

      //compute totals based on the data that is read from storage
      this.computeCartTotal();
    }
   }

  addToCart(cartItem: CartItem){
    let alreadyExistingInCart: boolean = false;
    let existingCartItem: CartItem = undefined;

    if(this.cartItems.length > 0){
     
      // for(let item of this.cartItems){
      //   if(item.id === cartItem.id){
      //     existingCartItem = item;
      //     break;
      //   }
      // }
      existingCartItem= this.cartItems.find(tempCartItem => tempCartItem.id === cartItem.id);
      
      alreadyExistingInCart = (existingCartItem != undefined);
    }

      if(alreadyExistingInCart){
        existingCartItem.quantity++;
      } else{
        this.cartItems.push(cartItem);
      }

    
    this.computeCartTotal();

  }
  computeCartTotal() {
    let totalPriceValue: number =0;
    let totalQuantityValue: number =0;

    for(let currentCartItem of this.cartItems){
      totalPriceValue += currentCartItem.unitPrice * currentCartItem.quantity;
      totalQuantityValue += currentCartItem.quantity;
    }
    //publish the new values... all subscribers will receive the new data
    this.totalPrice.next(totalPriceValue);
    this.totalQuantity.next(totalQuantityValue);

    //log the cart content for debuggin
    this.logCartData(totalPriceValue,totalQuantityValue);

    this.persistCartItems();
    
  }

  persistCartItems(){
    this.storage.setItem('cartItems', JSON.stringify(this.cartItems));
  }

  logCartData(totalPriceValue: number, totalQuantityValue: number) {

    console.log('Contents of the cart');
    for (let tempCartItem of this.cartItems) {
      const subTotalPrice = tempCartItem.quantity * tempCartItem.unitPrice;
      console.log(`name: ${tempCartItem.name}, quantity=${tempCartItem.quantity}, unitPrice=${tempCartItem.unitPrice}, subTotalPrice=${subTotalPrice}`);
    }

    console.log(`totalPrice: ${totalPriceValue.toFixed(2)}, totalQuantity: ${totalQuantityValue}`);
    console.log('----');
  }

  decrementQuantity(theCartItem: CartItem) {
    theCartItem.quantity--;

    if(theCartItem.quantity === 0){
      this.remove(theCartItem);
    } else{
      this.computeCartTotal();
    }
  }
  remove(theCartItem: CartItem) {
    let cartItemIndex = this.cartItems.findIndex(tempCartItem => tempCartItem.id === theCartItem.id);

    if(cartItemIndex > -1){
      this.cartItems.splice(cartItemIndex,1);
    }
    this.computeCartTotal();

  }
}
