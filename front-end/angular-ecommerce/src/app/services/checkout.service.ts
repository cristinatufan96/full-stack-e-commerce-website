import { PaymentInfo } from './../common/payment-info';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';
import { Purchase } from './../common/purchase';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  private purchaseUrl: string = environment.luv2codeApiUrl+`/checkout/purchase`;
  private paymentIntentUrl = environment.luv2codeApiUrl + '/checkout/payment-intent';

  constructor(private http: HttpClient) { }

  placeOrder(purchase: Purchase): Observable<any>{
    return this.http.post<Purchase>(this.purchaseUrl,purchase);
  }

  createPaymentIntent(paymentInfo: PaymentInfo): Observable<any>{
    return this.http.post<PaymentInfo>(this.paymentIntentUrl,paymentInfo);
  }
}
