import { environment } from './../../environments/environment';
import { OrderHistory } from './../common/order-history';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class OrderHistoryService {

private orderUrl: string = environment.luv2codeApiUrl+'/orders';

  constructor(private http: HttpClient) { }

  getOrderHistory(email: string):Observable<GetOrderHistoryResponse>{
    const searchUrl = `${this.orderUrl}/search/findByCustomerEmailOrderByDateCreatedDesc?email=${email}`;
     return this.http.get<GetOrderHistoryResponse>(searchUrl);

  }
}
interface GetOrderHistoryResponse{
  _embedded: {
    orders: OrderHistory[];
  }
}
