import { environment } from './../../environments/environment';
import { map } from 'rxjs/operators';
import { HashLocationStrategy } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { Country } from '../common/country';
import { State } from '../common/state';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  
  private countriesUrl: string = environment.luv2codeApiUrl+`/countries`;
  private statesUrl: string = environment.luv2codeApiUrl+`/states`;


  constructor(private http:HttpClient) { }

  getCreditCardMonths(startMonth: number): Observable<number[]> {
    let data: number[] = [];

    //build an array for "Month" dropdown list
    // - start at current month and loop until

    for (let theMonth = startMonth; theMonth <= 12; theMonth++) {
      data.push(theMonth);
    }
    return of(data);

  }

  getCreditCardYear(): Observable<number[]>{
    let data: number[] = [];

    //build an array for "Month" dropdown list
    // - start at current month and loop for next 10 years

    const startYear: number = new Date().getFullYear(); //Get the current year
    const endYear: number = startYear + 10;

    for(let theYear = startYear; theYear <=endYear; theYear++){
      data.push(theYear);
    }
    return of(data);

  }

  getCountries(): Observable<Country[]>{
    return this.http.get<GetResponseCountries>(this.countriesUrl).pipe(
      map(response => response._embedded.countries)
    );
  }

  getStates(theCountryCode: string): Observable<State[]>{


    const searchUrl = `${this.statesUrl}/search/findByCountryCode?code=${theCountryCode}`;
    return this.http.get<GetResponseStates>(searchUrl).pipe(
      map(response => response._embedded.states)
    );

  }
}
interface GetResponseCountries{
  _embedded: {
    countries: Country[];
  }
}

interface GetResponseStates{
  _embedded: {
    states: State[];
  }
}