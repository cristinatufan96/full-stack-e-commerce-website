import { environment } from './../../environments/environment';
import { Product } from './../common/product';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProductCategory } from '../common/product-category';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private url = environment.luv2codeApiUrl +'/products';
  private categoryUrl =environment.luv2codeApiUrl + '/product-category';


  constructor(private http: HttpClient) { }

  getProductListPaginate(thePage: number,
                        thePageSize: number,
                        theCategoryId: number): Observable<GetProductResponse> {
    const searchUrl = `${this.url}/search/findByCategoryId?id=${theCategoryId}`
      + `&page=${thePage}&size=${thePageSize}`;

    return this.http.get<GetProductResponse>(searchUrl);
  }
  searchProductsPaginate(thePageNumber: number,
                        thePageSize: number,
                        keyword: string): Observable<GetProductResponse> {
    const searchURL = `${this.url}/search/findByNameContaining?name=${keyword}`
      + `&page=${thePageNumber}&size=${thePageSize}`;

    return this.http.get<GetProductResponse>(searchURL);
  }

  getProducts(theCategoryId: number): Observable<Product[]> {
    const searchUrl = `${this.url}/search/findByCategoryId?id=${theCategoryId}`;

    return this.getProductsFromUrl(searchUrl);
  }

  getProductCategories(): Observable<ProductCategory[]> {
    return this.http.get<GetProductCategoriesResponse>(this.categoryUrl).pipe(
      map(response => response._embedded.productCategory)
    );
  }

  searchProducts(keyword: string): Observable<Product[]> {
    const searchURL = `${this.url}/search/findByNameContaining?name=${keyword}`;

    return this.getProductsFromUrl(searchURL);
  }

  private getProductsFromUrl(searchUrl: string): Observable<Product[]> {
    return this.http.get<GetProductResponse>(searchUrl).pipe(
      map(response => response._embedded.products)
    );
  }

  getProduct(theProductId: number): Observable<Product> {
    const theURL = `${this.url}/${theProductId}`;

    return this.http.get<Product>(theURL)
  }
}

interface GetProductResponse {
  _embedded: {
    products: Product[];
  },
  page: {
    size: number,
    totalElements: number,
    totalPages: number,
    number: number
  }
}
interface GetProductCategoriesResponse {
  _embedded: {
    productCategory: ProductCategory[];
  };
}
