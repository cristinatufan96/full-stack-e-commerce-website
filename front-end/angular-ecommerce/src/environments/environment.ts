// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  luv2codeApiUrl: "http://localhost:8080/api",
  stripePublishableKey: "pk_test_51LFylOCvUg5A66OzFvysE04ZO9QYbUmbaA3r43bXuUukKDUmiSDbkqrF1JDhieHGb245f5cfl1hQMAcwqfGqmLEa00iPm69mlF"
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
