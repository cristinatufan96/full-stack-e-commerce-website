package com.ecommerce.demo.services;

import com.ecommerce.demo.dto.PaymentInfo;
import com.ecommerce.demo.dto.Purchase;
import com.ecommerce.demo.dto.PurchaseResponse;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;

public interface CheckoutService {

    PurchaseResponse placeOrder(Purchase purchase);

    PaymentIntent createPaymentIntent(PaymentInfo paymentInfo) throws StripeException;
}
