package com.ecommerce.demo.entities;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "address")
@Setter
@Getter
@FieldDefaults(makeFinal = false,level = AccessLevel.PRIVATE)
public class Address {
    @Id
            @GeneratedValue(strategy = GenerationType.IDENTITY)
            @Column(name = "id")
    Long id;

    @Column(name = "city")
    String city;

    @Column(name = "country")
    String country;

    @Column(name = "state")
    String state;

    @Column(name = "street")
    String street;

    @Column(name = "zip_code")
    String zipCode;

    @OneToOne
            @PrimaryKeyJoinColumn
    Order order;
}
