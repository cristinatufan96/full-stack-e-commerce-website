package com.ecommerce.demo.entities;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "order_item")
@Setter
@Getter
@FieldDefaults(makeFinal = false,level = AccessLevel.PRIVATE)

public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "image_url")
    String imageUrl;

    @Column(name = "unit_price")
    BigDecimal unitPrice;

    @Column(name = "quantity")
    int quantity;

    @Column(name = "product_id")
    Long productId;

    @ManyToOne
    @JoinColumn(name = "order_id")
    Order order;



}
