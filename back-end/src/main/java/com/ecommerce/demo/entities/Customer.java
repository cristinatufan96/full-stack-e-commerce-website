package com.ecommerce.demo.entities;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "customer")
@Getter
@Setter
@FieldDefaults(makeFinal = false,level = AccessLevel.PRIVATE)
public class Customer {
    @Id
            @GeneratedValue(strategy = GenerationType.IDENTITY)
            @Column(name = "id")
    Long id;

    @Column(name = "first_name")
    String firstName;

    @Column(name = "last_name")
    String lastName;

    @Column(name = "email")
    String email;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    Set<Order> orders = new HashSet<>();

    public void add(Order order){
        if(order != null){

            if(orders == null){
                orders = new HashSet<>();
            }
            orders.add(order);
            order.setCustomer(this);
        }
    }
}
