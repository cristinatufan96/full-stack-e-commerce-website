package com.ecommerce.demo.dto;

import com.ecommerce.demo.entities.Address;
import com.ecommerce.demo.entities.Customer;
import com.ecommerce.demo.entities.Order;
import com.ecommerce.demo.entities.OrderItem;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Set;

@Data
@FieldDefaults(makeFinal = false,level = AccessLevel.PRIVATE)
public class Purchase {

    Customer customer;
    Address shippingAddress;
    Address billingAddress;
    Order order;
    Set<OrderItem> orderItems;
}
